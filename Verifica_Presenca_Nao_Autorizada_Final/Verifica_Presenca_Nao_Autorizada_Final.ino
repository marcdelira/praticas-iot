// UNIBRATEC - IOT
// Professsor: Cláudio Pereira
// Alunos: Marcello e Suedy
// ----------------------------------------------------------------------------------------------------------
// Exercicio para verificar presença não autorizada em um ambiente e enviar para uma app MQTT Dash os valores
// de referência captados por sensores de presença, som e ultrassônico para que o usuário acione um buzzer
// ----------------------------------------------------------------------------------------------------------
//
// Mapeamento do sensor de presença:
// Visto com os dois botões de ajustes para cima e o dipswitch amarelo à esquerda
// botão da esquerda: controla delay (tempo) - girando para direita (sentido do relógio) aumenta delay - totalmente para direita delay será de 5 minutos
//                                           - girando para a esquerda (contrário do relógio) diminui delay - totalmente para a esquerda delay será de 3 segundos
// botão da direita: controla sensibilidade - girando para direita (sentido do relógio) diminui sensibilidade - totalmente para direita sensibilidade será de 3 metros
//                                          - girando para esquerda (contrário do relógio) aumenta sensibilidade - totalmente para a esquerda sensibilidade será de 7 metros
// "Dip" mais à esquerda - VCC
// "Dip" do centro - OUT
// "Dip" mais à direita - GND
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// Mapeamento do sensor de som:
// Visto pela parte de cima com o microfone à direita e os "Dips" à esquerda
// "Dip" mais acima - saída analógica
// "Dip" segundo de cima para baixo - GND
// "Dip" terceiro de cima para baixo - VCC
// "Dip" mais abaixo - saída digital
// Botão de ajuste de sensibilidade do potenciômetro - girando para a esquerda aumenta sensibilidade
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//
// Mapeamento do sensor Ultrassônico:
// Visto pela parte de trás com os "Dip" para baixo
// "Dip" mais à esquerda - GND
// "Dip" segundo da esquerda para direita - Echo
// "Dip" terceiro da esquerda para direita - Trig
// "Dip" mais à direita - VCC
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#include <ESP8266WiFi.h>
#include <PubSubClient.h> 

const char* ssid = "xxxxxxxxxx";                // Colocar aqui o nome da sua rede local wifi
const char* password =  "xxxxxxxxxx";           // Colocar aqui a senha da sua rede local wifi
const char* mqttServer = "m1x.cloudmqtt.com";   // Colocar aqui o endereço do seu servidor fornecido pelo site
const int   mqttPort = 16000;                   // Colocar aqui a porta fornecida pelo site
const char* mqttUser = "xxxxxxxxxx";            // Colocar aqui o nome de usuario fornecido pelo site
const char* mqttPassword = "xxxxxxxxxx";        // Colocar aqui sua senha fornecida pelo site
const char *device_id = "esp8266";              // Colocar aqui um nome de device que desejar

WiFiClient espClient;
PubSubClient client(espClient);

const int BUZZER = 14;  // D5

// definições para o sensor de som
int pino_analogico = 0; // A0
int pino_digital = 16; // D0
int valor_A0 = 0;
int valor_D = 0;

// Definições para o ultrassom
const int trigPin = 5; // D1
const int echoPin = 4; // D2
int duracao;
int distancia;

// definição do pino de saída do sensor de presença
#define PIN_SENSOR 2 // D4 - sensor presença

// variável auxiliar para informar se o sensor de presença detectou algo, 1 = detectou, 0 = não detectou
int intrussao = 0;

void callback(char* topic, byte* dados_tcp, unsigned int length);

void setup() { 
  pinMode(BUZZER, OUTPUT);    
  analogWrite(BUZZER, 0);

  //Define pinos sensor de som como entrada
  pinMode(pino_analogico, INPUT);
  pinMode(pino_digital, INPUT);

  // Sensor ultrassônico
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);

  // Sensor presença
  pinMode(PIN_SENSOR, INPUT); 

  Serial.begin(115200);
 
  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) 
  {  delay(100);
     Serial.println("Tentando conectar com a rede Wi-Fi..");
  }
  Serial.println("Conseguiu conectar com a rede Wi-Fi...");  

  client.setServer(mqttServer, mqttPort);
  client.setCallback(callback);
 
  while (!client.connected()) {
    Serial.println("Tentando conectar ao servidor MQTT...");    
    if (client.connect(device_id, mqttUser, mqttPassword )) 
    { 
      Serial.println("Conseguiu conectar com o servidor MQTT...");  
       
    } else {
 
      Serial.print("Falha ao conectar MQTT ... ");
      Serial.print(client.state());
      delay(2000); 
    }
  }

  client.subscribe("esp8266/buzzer");
}
 
void callback(char* topic, byte* dados_tcp, unsigned int length) 
{
   String msg ; // cria variavel que será responsavel pelo tratamento das informações
   for(int i = 0; i < length; i++) //converte array de bytes em string
    {  char c = (char)dados_tcp[i];
       msg += c;
    } 

   if (strcmp(topic, "esp8266/buzzer") == 0)
   {
       for (int i = 0; i < length; i++) // Varre o vetor(array) inteiro do pacote recebido 
        {         
          //toma ação dependendo da string recebida:      
      
          if (msg.equals("1")) 
          {
                analogWrite(BUZZER, 500); 
          }
          if (msg.equals("0"))
          {
              analogWrite(BUZZER, 0);
          }
        }
   } 
}
 
void loop() {   
       
     // Verificações para o sensor de som
     valor_A0 = analogRead(pino_analogico);
     valor_D = digitalRead(pino_digital); 
     char str[10];
     sprintf(str, "Valor de Som = %d", valor_A0);
     client.publish("esp8266/som",str);    

     // Verificações para o sensor de ultrasom
     digitalWrite(trigPin, LOW);
     delayMicroseconds(2);   
     digitalWrite(trigPin, HIGH);
     delayMicroseconds(10); 
     digitalWrite(trigPin, LOW); 
     duracao = pulseIn(echoPin, HIGH);
     distancia = (duracao/2) / 29.1;
     char str2[10];
     sprintf(str2, "Valor do Ultrasom = %d", distancia);
     client.publish("esp8266/ultrasom",str2); 

     // faz a leitura do sensor de presença (retorna HIGH ou LOW)
     int sinal = digitalRead(PIN_SENSOR); 
     // HIGH : movimento detectado
     if(sinal == HIGH){
        intrussao = 1;
     }
     // LOW : sem movimento detectado
     else
     {
        intrussao = 0;
     }
     char str3[10];
     sprintf(str3, "Detecção de presença = %d", intrussao);
     client.publish("esp8266/presenca",str3);
     
     client.loop();
}
