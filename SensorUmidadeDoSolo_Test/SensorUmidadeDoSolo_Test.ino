#define sensorUmidade A0 // Sensor de umidade de solo do módulo
#define pinRele D3
unsigned long timer = 0;
int leitura;
bool irrigacaoAtiva = false;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  delay(1000);
  pinMode(pinRele, OUTPUT);
  digitalWrite(pinRele, HIGH);
}

void loop() {
  // put your main code here, to run repeatedly:
  if((millis() - timer) >= 300) {
    
    leitura = analogRead(sensorUmidade);
    showSoilHumidityTest();
    
    if (leitura >= 900) {
      Serial.println("leitura acima de 900");
      if (!irrigacaoAtiva) {
        digitalWrite(pinRele, !digitalRead(pinRele));
        irrigacaoAtiva = true;
        Serial.println("Ativando sistema de irrigação");
      }
    } else {
      if (leitura <= 390) {
        Serial.println("leitura abaixo de 340");
        if (irrigacaoAtiva) {
          digitalWrite(pinRele, !digitalRead(pinRele));
          irrigacaoAtiva = false;
          Serial.println("Desativando sistema de irrigação");
        }
      }
    }
    timer = millis();
  }
}

void showSoilHumidity() {
  leitura = analogRead(sensorUmidade); // Leitura dos dados analógicos vindos do sensor de umidade de solo
 
  if (leitura <= 1023 && leitura >= 682) { // Se a leitura feita for um valor entre 1023 e 682 podemos definir que o solo está com uma baixa condutividade, logo a planta deve ser regada
    Serial.println("Nível de Umidade Baixo");
  } else {
    if (leitura <= 681 && leitura >= 341) { // Se a leitura feita for um valor entre 681 e 341 podemos definir que o solo está com um nível médio de umidade, logo dependendo da planta pode ou não ser vantajoso regar
    Serial.println("Nível de Umidade Médio");
    }
    else {
      if (leitura <= 340 && leitura >= 0) { // Se a leitura feita for um valor entre 0 e 340 podemos definir que o solo está com um nível aceitável de umidade, logo talvez não seja interessante regar neste momento
        Serial.println("Nível de Umidade Alto");
      }
    } 
  }  
}

void showSoilHumidityTest() {
  Serial.println(analogRead(sensorUmidade));
}
