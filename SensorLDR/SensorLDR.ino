const int seletor = A3;
int ledPin = 8;
int ldrPin = A1;
int ldrValor = 0;

void setup() {
  // put your setup code here, to run once:
  pinMode(ledPin, OUTPUT);
  pinMode(seletor, OUTPUT);
  Serial.begin(9600);

}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(seletor, HIGH);
  ldrValor = analogRead(ldrPin);

  if (ldrValor >= 200) {
    digitalWrite(ledPin, HIGH);
  } else {
    digitalWrite(ledPin, LOW);
  }

  Serial.println(ldrValor);
  delay(100);
}
