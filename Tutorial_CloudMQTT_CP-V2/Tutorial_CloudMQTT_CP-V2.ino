#include <ESP8266WiFi.h>
#include <PubSubClient.h> // Biblioteca usada, baixe e instale se não a tiver, link abaixo
                          //https://github.com/knolleary/pubsubclient/blob/master/examples/mqtt_esp8266/mqtt_esp8266.ino

//Define a pinagem do ESP8266

#define D5    14
//#define D6    12
//#define D7    13

const char*         ssid = "xxxxxx"; //Aqui o nome da sua rede local wi fi
const char*     password = "xxxxxx"; // Aqui a senha da sua rede local wi fi
const char*   mqttServer = "m15.cloudmqtt.com"; // Aqui o endereço do seu servidor fornecido pelo site 
const int       mqttPort = 15950; // Aqui mude para sua porta fornecida pelo site
const char*     mqttUser = "bspouqyb"; //  Aqui o nome de usuario fornecido pelo site
const char* mqttPassword = "xSqOx3aIN1T_"; //  Aqui sua senha fornecida pelo site
char         EstadoSaida = '0';  

WiFiClient espClient;
PubSubClient client(espClient);

void mqtt_callback(char* topic, byte* payload, unsigned int length);

void setup() {
  pinMode(D5, OUTPUT);
  digitalWrite(D5, LOW); 
//  pinMode(D6, OUTPUT);
//  digitalWrite(D6, LOW); 
//  pinMode(D7, OUTPUT);
//  digitalWrite(D7, LOW); 

  Serial.begin(115200);
 
  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.println("Connecting to WiFi..");
  }
  Serial.println("Connected to the WiFi network");
 
  client.setServer(mqttServer, mqttPort);
  client.setCallback(mqtt_callback);
 
  while (!client.connected()) {
    Serial.println("Connecting to MQTT...");
 
    if (client.connect("ESP8266Client", mqttUser, mqttPassword )) {
 
      Serial.println("connected");  
 
    } else {
 
      Serial.print("failed with state ");
      Serial.print(client.state());
      delay(2000);
    }
  }
  
 
  //client.publish("liga","desliga");
  client.subscribe("LED");
//  client.subscribe("LED5");
//  client.subscribe("LED6");
//  client.subscribe("LED7");
}
 
void mqtt_callback(char* topic, byte* payload, unsigned int length) {
 
  Serial.print("Message arrived in topic: ");
  Serial.println(topic);
 
  Serial.print("Message:");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    String msg;
    
    //obtem a string do payload recebido
    for(int i = 0; i < length; i++) {
       char c = (char)payload[i];
       msg += c;
    }
        
    //toma ação dependendo da string recebida:
    //verifica se deve colocar nivel alto de tensão na saída.
    //IMPORTANTE: o Led já contido na placa é acionado com lógica invertida (ou seja,
    //enviar HIGH para o output faz o Led apagar / enviar LOW faz o Led acender)
  
    //verifica se deve colocar nivel alto de tensão na saída se enviar L e digito, ou nivel baixo se enviar D e digito no topíco LED
    if (msg.equals("L5")) {
        digitalWrite(D5, HIGH);
        EstadoSaida = '0';
    }
    if (msg.equals("D5")) {
        digitalWrite(D5, LOW);
        EstadoSaida = '1';
    }
  }

  Serial.println();
  Serial.println("-----------------------");
}
 
void loop() {
  client.loop();
}
