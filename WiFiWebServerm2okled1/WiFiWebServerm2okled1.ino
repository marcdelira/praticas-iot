
#include <ESP8266WiFi.h>

#define ldr 0
#define led1 14
#define led2 12
#define led3 13 // 13 nodemcu e D1 ou 16 d1 mini

const char* ssid = "GVT-E92E";
const char* password = "CP1137SM3NB";

WiFiServer server(80);



void setup() {
  Serial.begin(115200);
  delay(10);

//  pinMode(ldr, INPUT);
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);

  digitalWrite(led1, HIGH);
 
  // Connect to WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  
  // Start the server
  server.begin();
  Serial.println("Server started");

  // Print the IP address
  Serial.println(WiFi.localIP());
}

void loop() {
  // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
  
  // Wait until the client sends some data
  Serial.println("new client");
  while(!client.available()){
    delay(1);
  }
  
  // Read the first line of the request
  String request = client.readStringUntil('\r');
  Serial.println(request);
  client.flush();

   if(request.indexOf("led1") != -1){
      digitalWrite(led1, !digitalRead(led1));
   }
  if(request.indexOf("led2") != -1){
      digitalWrite(led2, !digitalRead(led2));
   }
  if(request.indexOf("led3") != -1){
      digitalWrite(led3, !digitalRead(led3));
   }
  
  if(request.indexOf("todos") != -1){
      digitalWrite(led1, LOW);
      digitalWrite(led2, LOW);
      digitalWrite(led3, LOW);
   }

   if(request.indexOf("ligar") != -1){
      digitalWrite(led1, HIGH);
      digitalWrite(led2, HIGH);
      digitalWrite(led3, HIGH);
   }
  
//  int luminosidade = analogRead(ldr);
  int luminosidade = 0;
   
  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/html");
  client.println("");

  if(digitalRead(led1)) {
    client.print("l1on");
  } else {
    client.print("l1of");
  }
  client.print(",");

  if(digitalRead(led2)) {
    client.print("l2on");
  } else {
    client.print("l2of");
  }
  client.print(",");
  
  if(digitalRead(led3)) {
    client.print("l3on");
  } else {
    client.print("l3of");
  }
  client.print(",");
  
  client.print(luminosidade);
  
  delay(1);
}
  
  
  

  
  

