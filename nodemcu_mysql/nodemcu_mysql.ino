#include <ESP8266WiFi.h>

const char* ssid = "Unibratec";
const char* pass = "";

const char* host = "172.16.1.190";

long sensor1 = 0;
long sensor2 = 0;
long sensor3 = 0;

void setup() {
  Serial.begin(115200);
  delay(10);

  // Connect to WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, pass);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  
  // Print the IP address
  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());
}

void loop() {

  Serial.print("Conectando com ");
  Serial.println(host);
  
  // simulando leitura dos sensores
  sensor1 = random(100);
  sensor2 = random(100);
  sensor3 = random(100);

  Serial.print("sensor1: ");
  Serial.print(sensor1);
  Serial.print("sensor2: ");
  Serial.print(sensor2);
  Serial.print("sensor3: ");
  Serial.print(sensor3);

  WiFiClient client;
  
  const int httpPort = 80;
  if (!client.connect(host, httpPort)) {
    Serial.println("Falha na conexão");
    return;
  }

  String url = "/nodemcu/salvar.php?";
  url += "sensor1=" + sensor1;
  url += "&sensor2=" + sensor2;
  url += "&sensor3=" + sensor3;

  Serial.print("Requisitanto a url: ");
  Serial.println(url);
/*
  // this will send the request to the server
  client.print(String("GET ") + url + "HTTP/1.1\r\n" + 
    "Host: " + host + "\r\n" +
    "Connection: close\r\n\r\n");

  unsigned long timeout = millis();
  while (client.available() == 0) {
    if (millis() - timeout > 5000) {
      Serial.println(">>> Client timeout!");
      client.stop();
      return;
    }
  }

  // Read all the lines of the reply from de server and print them to Serial
  while(client.available()) {
    String line = client.readStringUntil('\r');
    Serial.print(line);
  }
*/
  Serial.println();
  Serial.println("Conexão encerrada");

  delay(10000);

  
}







